# AtigaOS #

### Initialize ###

```bash
repo init --depth=1 --no-repo-verify -u https://gitlab.com/AtigaOS/manifest.git -b 1.0 -g default,-mips,-darwin,-notdefault
```

### Sync ###

```bash
repo sync -c --no-clone-bundle --no-tags --optimized-fetch --prune --force-sync -j$(nproc --all)
```

### Build ###

```bash

# Set up environment
$ . build/envsetup.sh

# Choose a target
$ lunch atiga_$device-userdebug

# Build the code
$ mka atiga -jX
```
